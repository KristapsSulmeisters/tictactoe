import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
         Scanner keybInput = new Scanner(System.in);
         Game game = new Game();
         game.initializeBoard();
         String player = "X";
         while(true){
             System.out.println(game.printBoard());
             System.out.println("enter coordinates for row:\n>");

             int row = keybInput.nextInt();
             System.out.println("enter coordinates for col:\n>");

             int col = keybInput.nextInt();

             game.play(row, col, player);
             if(game.gameOver()){
                 System.out.println(game.printBoard() + "\n" + player + " wins" );
                break;
             }

             if(player==("X")){
                 player=("O");
             }else {
                 player=("X");
             }
         }


    }


}
